@lazyglobal off.

local cfg_margin is 0.99.
local cfg_steady is 0.01.

function steer_config {
	parameter margin is 0.99. // angle cos margin
	parameter steady is 0.01. // momentum magnitude margin
	set cfg_margin to margin.
	set cfg_steady to steady.
}

/// lock steering, and return immediately.
function steer {
	parameter param.
	steer_wait(param, 0).
}

/// lock steering, and return when facing vector is stabilized
function steer_wait {
	parameter where.
	parameter timeout is -1.
	return steer_wait_until(where, {
		parameter target.
		return vdot(ship:facing:vector, target) > cfg_margin
			and ship:angularmomentum:mag < cfg_steady.
	}, timeout).
}

/// lock steering, and return when facing vector is approached (stabilization not guaranteed).
function steer_wait_facing {
	parameter where.
	parameter timeout is -1.
	return steer_wait_until(where, {
		parameter target.
		return vdot(ship:facing:vector, target) > cfg_margin.
	}, timeout).
}

function steer_kill {
	sas off.
	lock steering to "kill".
}

function steer_off {
	sas off.
	unlock steering.
}

function steer_sas {
	unlock steering.
	sas on.
}

// bug: ~~if param is a function~~, it may be called twice
function steer_wait_until {
	parameter param.
	parameter condition.
	parameter timeout is -1.

	local dir is ship:facing.
	local vec is ship:facing:vector.
	local steering_f is {
		local where is param().
		if where:typename = "Vector" {
			set where to lookdirup(where, ship:facing:topvector).
		}
		set dir to where.
		set vec to where:vector:normalized.
		return where.
	}.

	if timeout < 0 {
		set timeout to time:seconds + 1e100.
	}

	lock steering to steering_f().

	until time:seconds > timeout {
		if condition(vec) { break. }
		wait 0.
	}
}

local function steer_f_wait_until {
	parameter f.
	parameter condition.
	parameter timeout is -1.

	lock steering to f().
}
