@lazyglobal off.

local dat_file is "1:/program.dat".
local state_file is "1:/state.dat".

local cfg_halt is false.

local last_step_command is 0.
local last_step_started is 0.
if exists(dat_file) {
	set dat_file to open(dat_file).
	set last_step_started to dat_file:readall():string():tonumber().
} else {
	set dat_file to create(dat_file).
	dat_file:writeln("0").
}

global function program_step {
	parameter name.
	parameter body_callback.

	if cfg_halt { return. }

	set last_step_command to last_step_command + 1.
	if last_step_command >= last_step_started {
		set last_step_started to last_step_command.
		dat_file:clear().
		dat_file:writeln(""+last_step_started).
		print name.
		body_callback:call().
	}
}

global function program_halt {
	set cfg_halt to true.
}

global function program_save_state {
	parameter state.
	writejson(state, state_file).
}

global function program_load_state {
	if exists(state_file) {
		return readjson(state_file).
	}
	return lexicon().
}
