@lazyglobal off.

local defaults is lexicon(
	"launch_pitch:45", 80,
	"launch_pitch:30", 60,
	"active_slowdown", false,
	"wobble", false,
	"close_enough", {return true.},
	"bay_always_open", false,
	"deltav_drop", 1e100,
	"chutes_altitude", 35000
).

clearguis().
local profiles is lexicon(
	"Badlands", lexicon(
		"required_flags", list("30"),
		"launch_azimuth", 115,
		"target_geopos", latlng(-24, 10.5),
		"active_slowdown", true,
		"wobble", true
	),
	"North pole", lexicon(
		"launch_azimuth", -2,
		"target_geopos", latlng(75, -74.55),
		"active_slowdown", true,
		"wobble", true
	),
	"South pole", lexicon(
		"launch_azimuth", 182,
		"target_geopos", latlng(-80, -74.55),
		"active_slowdown", true,
		"wobble", true
	),
	"Tundra", lexicon(
		"launch_azimuth", -2,
		"launch_azimuth:30", -4,
		"target_geopos", latlng(70.663, -85.15),
		"active_slowdown", true,
		"wobble", true
	),
	"Ice caps", lexicon(
		"launch_azimuth", -3,
		"launch_azimuth:30", -7,
		"target_geopos", latlng(72.069, -93.052),
		"active_slowdown", true,
		"wobble", true
	),
	"Desert", lexicon(
		"launch_pitch:45", 70,
		"launch_azimuth", -90,
		"target_geopos", latlng(0, -88),
		/// minimum distance to expect before checking distance to target being decreased
		"close_enough", {
			return addons:tr:hasimpact and addons:tr:impactpos:lng < -85.
		},
		"bay_always_open", true,
		"wobble", true,
		/// drop extra fuel
		"deltav_drop", 1500
	),
	"Mountains", lexicon(
		"launch_pitch:45", 70,
		"launch_azimuth", -92,
		"target_geopos", latlng(-0.25, -79.4),
		/// minimum distance to expect before checking distance to target being decreased
		"close_enough", {
			return addons:tr:hasimpact and addons:tr:impactpos:lng < -79.4.
		},
		"bay_always_open", true,
		/// drop extra fuel
		"deltav_drop", 1100
	),
	"Highlands", lexicon(
		"launch_pitch", 60,
		"launch_azimuth", -45,
		"target_geopos", latlng(2.1, -76.7),
		"bay_always_open", true,
		"deltav_drop", 1000,
		"chutes_altitude", 6500
	),
	"Grasslands", lexicon(
		"launch_pitch", 60,
		"launch_azimuth", -90,
		"target_geopos", latlng(0, -75.5),
		"bay_always_open", true,
		"deltav_drop", 600,
		"chutes_altitude", 6500
	),
	"Water", lexicon(
		"launch_pitch", 60,
		"launch_azimuth", 90,
		"target_geopos", latlng(0, -73.5),
		"bay_always_open", true,
		"deltav_drop", 600,
		"chutes_altitude", 6500
	),
	"Sea shelf", lexicon(
		"launch_pitch", 60,
		"launch_azimuth", 0,
		"target_geopos", latlng(1, -74.5),
		"bay_always_open", true,
		"deltav_drop", 600,
		"chutes_altitude", 6500
	)
).

global function sky_select_gui {
	parameter flags.

	local profile is 0.
	local g is gui(250).
	local h is g:addHLayout().
	local l is h:addLabel("Launch to destination:").
	set l:style:align to "center".
	local b is h:addButton("R").
	set b:onClick to {
		reboot.
	}.
	local cl is h:addButton("✕").
	set cl:onClick to {
		set profile to -1.
		program_halt().
	}.
	for dest in profiles:keys {
		local title is dest.
		if profiles[dest]:haskey("deltav_drop") {
			set title to title + " (" + profiles[dest]:deltav_drop + " m/s)".
		}
		local b is g:addButton(title).
		local dest_key is dest.
		set b:onClick to { set profile to dest_key. }.

		if profiles[dest]:haskey("required_flags") {
			for f in flags {
				if not profiles[dest]:required_flags:contains(f) {
					set b:enabled to false.
					break.
				}
			}
		}
	}
	set g:y to 300.
	g:show().
	wait until profile <> 0.
	program_save_state(lexicon("profile", profile)).
	g:dispose().
}

global function sky_select_load {
	parameter flags is list().

	local state is program_load_state().
	local profile is profiles[state:profile].
	local defs is flag_override(defaults, flags).
	// apply defaults:
	for key in defs:keys() {
		if not profile:haskey(key) {
			set profile[key] to defs[key].
		}
	}
	local profile is flag_override(profile, flags).
	return profile.
}

local function flag_override {
	parameter lex, flags.
	local res is lexicon().
	local overrides is lexicon().
	for key in lex:keys() {
		if key:contains(":") {
			local ff is key:split(":").
			local kk is ff[0].
			from { local i is 1. } until i >= ff:length step { set i to i + 1. } do {
				local flag is ff[i].
				if flags:contains(flag) {
					if overrides:haskey(kk) and overrides[kk] <> lex[key] {
						print "Warning: propery conflict for "+kk+
							". Assuming value "+overrides[kk]+".".
					} else {
						set overrides[kk] to lex[key].
					}
				}
			}
		} else {
			set res[key] to lex[key].
		}
	}
	for key in overrides:keys() {
		set res[key] to overrides[key].
	}
	return res.
}

global function sky_select_dump {
	parameter profile.

	for key in profile:keys() {
		print key + ": " + profile[key].
	}
}
