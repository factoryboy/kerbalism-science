@lazyglobal off.

runoncepath("0:/lib/program0.ks").
runoncepath("0:/lib/steer0.ks").
runoncepath("parts.ks").
runoncepath("select.ks").

local flags is list().
if ship:partsnamed("liquidEngine"):length > 0 {
	flags:add("30"). // LV-T30
} else {
	flags:add("45"). // LV-T45
}
program_step("Destination GUI", {
	sky_select_gui(flags).
}).

local profile is sky_select_load(flags).
sky_select_dump(profile).
if profile:bay_always_open {
	servicebay_open().
}
clearvecdraws().
local vd is vecdraw(
	{return body:position.},
	{return profile:target_geopos:altitudeposition(200000) - body:position.},
	green, "", 1, 1, 0.1, 0
).

program_step("DeltaV Check", {
	steer({return ship:up:vector.}).
	lock throttle to 0.
	until ship:maxthrust > 0 {
		stage.
		wait 0.
	}
	local isp is engine_isp().
	local function deltav {
		local density is 0.005. // both LF and Ox
		local fuel is min(ship:liquidfuel / 9, ship:oxidizer / 11).
		local fuel_mass is fuel * (9 + 11) * density.
		return engine_isp() * constant:g0 * ln(ship:mass / (ship:mass - fuel_mass)).
	}
	if deltav() > profile:deltav_drop {
		lock throttle to 0.9 *
			(body:mu * ship:mass / (body:radius + altitude)^2)
			/ ship:maxthrust.
		wait until deltav() < profile:deltav_drop.
	}
}).

// At target speed steer to target pitch and azimuth.
program_step("Launch", {
	antennae(false).
	local t_speed is 100.
	local t_pitch is profile:launch_pitch.
	local t_azimuth is profile:launch_azimuth.

	until ship:maxthrust > 0 {
		stage.
		wait 0.
	}
	lock throttle to 1.
	until false {
		wait 0.
		local progress is ship:velocity:surface:mag / t_speed.
		if progress >= 1 { break. }
		local pitch is 90 + progress * (t_pitch - 90).
		steer({ return heading(t_azimuth, pitch). }).
	}
	// wait until target pitch is reached by velocity vector.
	wait until ship:up:vector * ship:velocity:surface:normalized < sin(t_pitch).
}).

program_step("Gravity turn", {
	lock throttle to 1.
	steer({ return ship:velocity:surface. }).

	set addons:tr:prograde to true.
	local impact_dist is 1e100.
	local target is profile:target_geopos.
	// Burn until impact point starts moving away from the target for 4 ticks straight.
	local tick_count is 0.
	//lock throttle to 2^(-tick_count).
	until ship:maxthrust = 0 {
		wait 0.
		if addons:tr:hasimpact {
			local pos is addons:tr:impactpos.
			local dist is (pos:altitudeposition(1e6) - target:altitudeposition(1e6)):mag.

			if dist - impact_dist > 0.1 {
				print dist - impact_dist.
				set tick_count to tick_count + 1.
				if tick_count >= 4 and profile:close_enough() {
					print "Target was reached".
					break.
				}
			} else {
				set impact_dist to dist.
				set tick_count to 0.
			}
		}
	}
	//set vd:show to false.
	lock throttle to 0.
	steer_off().
}).

program_step("Free ascent", {
	when ship:verticalspeed < 0 or ship:altitude > 55000 then {
		servicebay_open().
	}
	when ship:altitude > 60000 then {
		antennae(true).
	}
	when ship:verticalspeed < 0 or ship:altitude > 55000 then {
		servicebay_open().
	}
	wait until ship:verticalspeed < 0.
}).

program_step("Descent", {
	when ship:altitude < 55000 then {
		antennae(false).
	}
	wait until ship:verticalspeed < 0.
	if profile:active_slowdown or profile:wobble {
		wait until ship:altitude < 70000.
		steer({return -ship:velocity:surface.}).
	}
	wait until ship:altitude < 35000.
	if profile:active_slowdown {
		lock throttle to 1.
		wait until ship:maxthrust = 0.
		lock throttle to 0.
	}
	wait until alt:radar < profile:chutes_altitude.
	stage. // chutes
	steer_off().
	if profile:wobble {
		print " -- wobbling".
		set ship:control:pilotpitchtrim to 1.
		set ship:control:pilotrolltrim to 1.
		wait until ship:velocity:surface:mag < 400.
		set ship:control:pilotpitchtrim to 0.
		set ship:control:pilotrolltrim to 0.
	}
	wait until ship:velocity:surface:mag < 15.
	antennae(true).
	// TODO orient along the ground
	// TODO dump extra fuel on desert and mountain landings, to get
	// longer atm science gathering period
	wait until alt:radar < 50.
	set kuniverse:timewarp:rate to 1.
}).

program_step("EC control", {
	wait until ship:electriccharge < 10.
	fuel_cells(true).
	shutdown.
}).
