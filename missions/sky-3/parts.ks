@lazyglobal off.

function fuel_cells {
	parameter onoff.

	local look_for_event is "fuel cell</b>: running".
	if onoff {
		set look_for_event to "fuel cell</b>: stopped".
	}

	for p in ship:parts {
		from { local i is 0. } until i >= p:allmodules:length step { set i to i + 1. } do {
			local m is p:getmodulebyindex(i).
			if m:name = "ProcessController" {
				for ev in m:alleventnames {
					if ev:contains(look_for_event) {
						for ac in m:allactionnames {
							if ac:startswith("start/stop")
								and ac:endswith("fuel cell")
							{
								m:doaction(ac, true).
								break.
							}
						}
						break.
					}
				}
			}
		}
	}
}

function servicebay_open {
	for p in ship:partsnamed("ServiceBay.125.v2") {
		local m is p:getmodule("ModuleAnimateGeneric").
		if m:hasevent("open") {
			m:doevent("open").
		}
	}
}

function antennae {
	parameter extend.
	for p in ship:parts {
		if p:hasmodule("ModuleDeployableAntenna") {
			local m is p:getmodule("ModuleDeployableAntenna").
			if extend {
				m:doaction("extend antenna", true).
			} else {
				m:doaction("retract antenna", true).
			}
		}
	}
}

function engine_isp {
	for p in ship:parts {
		if p:hasmodule("ModuleEngines") {
			return p:isp.
		}
	}
}
